<%-- 
    Document   : index
    Created on : 02-may-2015, 0:05:24
    Author     : Cesar Lopez
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrar Usuario</title>

    </head>
    <body>
        <h1>TRANSCOR</h1>
        <s:form action="addUsuarioXML">
            <s:textfield label="usuario" name="usuario" placeholder="Email" required="required"  ></s:textfield>
            <s:textfield label="clave" name="clave" placeholder="ingrese una clave" required="required"></s:textfield>
            <s:textfield label="numero de boleta" name="numeroBoleta" placeholder="ingrese el numero de boleta" required="required"  ></s:textfield>
            <s:textfield label="observacion " name="observacion" placeholder="Observacion" ></s:textfield>
            <s:submit  value="Crear"></s:submit>
        </s:form>
    </body>
    <s:actionerror />
    <s:actionmessage />
</html>
