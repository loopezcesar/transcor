package com.transcor.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {
    
    public static Connection getConnection() throws Exception{
        Connection Cnn = null;
        try{
            String Url = "jdbc:mysql://localhost:3306/transcor";
            String User = "root";
            String Password = "123456";
            Class.forName("com.mysql.jdbc.Driver");
            Cnn = DriverManager.getConnection(Url, User, Password);
            System.out.println("Exito Conexion BD");
        }catch(Exception e){
            System.out.println("Error Conexion BD");
        }
        return Cnn;
    }
}
