/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transcor.dao;


import com.transcor.bean.Usuario;
import com.transcor.service.UsuarioInterfaz;
import java.sql.CallableStatement;
import java.sql.Connection;
/**
 *
 * @author Cesar Lopez
 */
public class UsuarioDAO implements UsuarioInterfaz{

    @Override
    public boolean loginUsuario(String User, String Clave, String numeroBoleta, String observacion) throws Exception {
        Connection con=null;
        CallableStatement cstm=null;
        try {
            System.out.println("entro a TRY del UsuarioDAO - loginUsuario");
            con=Conexion.getConnection();
            String sql="CALL sp_addUsuario(?,?,?,?)";
            cstm=con.prepareCall(sql);
            cstm.setString(1,User);
            cstm.setString(2,Clave);
            cstm.setString(3,numeroBoleta);
            cstm.setString(4,observacion);
            cstm.execute();
            System.out.println(" exito UsuarioDAO -------> loginUsuario");
            return true;
        } catch (Exception e) {
            System.out.println(" error UsuarioDAO -------> loginUsuario "+e.getMessage());
            e.printStackTrace();
            return false;
        }finally{
            cstm.close();
            con.close();
        }
    }

   
    
}
