/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.transcor.action;

import com.opensymphony.xwork2.ActionSupport;
import com.transcor.dao.UsuarioDAO;

/**
 *
 * @author Cesar Lopez
 */
public class MetodosAction extends ActionSupport {
    
    
    String usuario, clave, numeroBoleta, observacion;
    
    
    public String nuevoUsuario() throws Exception {
        boolean res = false;
        try{
            UsuarioDAO usuarioDao = new UsuarioDAO();
            res = usuarioDao.loginUsuario(usuario, clave, numeroBoleta, observacion);
        }catch(Exception e){
            System.out.println("error en el action ------> nuevoUsuario"+e.getMessage());
            e.printStackTrace();
        }
        if(res == true){
            addActionMessage("Usuario Registrado correctamente :) ");
            return SUCCESS;
        }else{
             addActionMessage("Error al registrarlo como nuevo usuario, intelo nuevamente por favor");
            return ERROR;
        }
    }
    
   /* public MetodosAction() {
    }
    
    public String execute() throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }*/
    
    
    
    
    
    
    /*------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------*/
    /*--------------------------GET AND SET ----------------------------------*/
    /*------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------*/
    
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNumeroBoleta() {
        return numeroBoleta;
    }

    public void setNumeroBoleta(String numeroBoleta) {
        this.numeroBoleta = numeroBoleta;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    
    
    
    
}
